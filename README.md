## Semana 21 - Paypal

1. Intro a Paypal
2. Corriendo el proyecto
3. Creando la cuenta Paypal
4. Guardando las claves
5. Cargando las variables automáticamente
6. Variables de entorno en Heroku
7. Documentación de Paypal
8. El modelo de Biling
9. Detalles del modelo y controller
10. Preparación del pago
11. Obteniendo el total con pluck
12. Generando el hash para Paypal
13. Ingresando el pago en Paypal
14. Ejecutando el pago
15. Guardando el pago en la base de datos
16. Listando pagos guardados
17. Refactoring con Scope
18. Refactoring con método de clase
19. Refactoring con método privado
20. Url para producción

https://developer.paypal.com

Ruby SDK for PayPal RESTful APIs: https://github.com/paypal/PayPal-Ruby-SDK

Rails ERD: https://github.com/voormedia/rails-erd

Run $ bundle exec erd'